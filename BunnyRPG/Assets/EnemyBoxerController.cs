﻿using UnityEngine;
using System.Collections;
using System.Globalization;

public class EnemyBoxerController : MonoBehaviour
{

    enum States { Idle, Punching, Blocking, IsBeingHit};

    public int state = 0;
    public bool isPunching = false;
    public bool isHitting = false;
    public int minPunchWaitTime = 5;
    public int maxPunchWaitTime = 10;
    private Animator animator;
    private bool waitingRandomTime = false;
    public Animator damageAnimation;
    public float defaultPunchPercentage = 0.66f;
    private bool cannotBeHit = false;
    public EnemyBoxerController opponent;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        StartCoroutine(waitRandomTimeForPunchOrBlock(minPunchWaitTime, maxPunchWaitTime, defaultPunchPercentage));
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (Input.GetKeyDown("space"))
	    {
	        attemptedBeingPunched();
	    }
	}

    IEnumerator waitRandomTimeForPunchOrBlock(int minSeconds, int maxSeconds, float punchPercentage)
    {
        int wait_time = Random.Range(minSeconds, maxSeconds);
        Debug.Log("Waiting for " + wait_time + " seconds");
        yield return new WaitForSeconds(wait_time);
        Debug.Log("Finished waiting");
        if (animator.GetInteger("state") == (int) States.Idle)
        {
            bool willPunch = (Random.Range(0.0f, 1.0f) <= punchPercentage);
            if (willPunch)
            {
                animator.SetInteger("state", (int) States.Punching);
            }
            else
            {
                animator.SetInteger("state", (int) States.Blocking);
            }
            
        }
    }

    public void setIsHitting()
    {
        Debug.Log("This is where damage would be taken");
        isHitting = true;
        opponent.attemptedBeingPunched();
       
    }

    public void setCannotBeHit()
    {
        Debug.Log("This is where damage cannot be taken");
        cannotBeHit = true;
    }

    public void setCanBeHit()
    {
        Debug.Log("Damage can be taken again");
        cannotBeHit = false;
    }

    public void backToIdle()
    {
        isHitting = false;
        animator.SetInteger("state", (int)States.Idle);
        Debug.Log("Back to idle state, value " + animator.GetInteger("state"));
        StartCoroutine(waitRandomTimeForPunchOrBlock(minPunchWaitTime, maxPunchWaitTime, defaultPunchPercentage));
        cannotBeHit = false;
    }

    public void attemptedBeingPunched()
    {
        if (!cannotBeHit)
        {
            animator.SetInteger("state", (int)States.IsBeingHit);
            damageAnimation.SetTrigger("damaged");
        }
    }
}
