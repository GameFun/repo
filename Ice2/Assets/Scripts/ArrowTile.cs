﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowTile : MonoBehaviour, ITile
{

    public Coordinate gridLocation { get; set; }
    public Transform getTransform { get { return transform; } }
    public char newDirection;
    private Coordinate coordinateDirection = new Coordinate(1,0);

    public void Awake()
    {
        this.gridLocation = gridLocation;
        print(newDirection);
        switch (newDirection)
        {
            case 'n': case 'N':
                coordinateDirection = new Coordinate(0, 1);
                break;
            case 's': case 'S':
                coordinateDirection = new Coordinate(0, -1);
                break;
            case 'e': case 'E':
                coordinateDirection = new Coordinate(1, 0);
                break;
            case 'w': case 'W':
                coordinateDirection = new Coordinate(-1, 0);
                break;
            default:
                print("Incorrect direction given to ArrowTile. This will fail");
                break;
        }
    }

    //return the tile at the full end of the movement in the new direction
    public Coordinate getNewDirection(Coordinate currentDirection)
    {
        return coordinateDirection;
    }

    public bool causesMovementChange()
    {
        return true;
    }

    public bool stopsMovement()
    {
        return false;
    }

    public bool canBeOnTile()
    {
        return true;
    }
}
