﻿using UnityEngine;
using System.Collections;

public class GoalTile : MonoBehaviour, ITile {

    public Coordinate gridLocation { get; set; }
    public Transform getTransform { get { return transform; } }

    public Coordinate getNewDirection(Coordinate currentDirection)
    {
        return currentDirection;
    }

    public bool causesMovementChange()
    {
        return true;
    }

    public bool stopsMovement()
    {
        return true;
    }

    public bool canBeOnTile()
    {
        return true;
    }
}
