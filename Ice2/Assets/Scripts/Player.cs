﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public Coordinate gridLocation {get; set;}
    public Coordinate directionOfMovement { get; set; }

    public Player(Coordinate location)
    {
        this.gridLocation = location;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
