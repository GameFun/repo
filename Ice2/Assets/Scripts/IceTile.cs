﻿using System;
using UnityEngine;
using System.Collections;

public class IceTile : MonoBehaviour, ITile {

    public Coordinate gridLocation { get; set; }
    public Transform getTransform { get { return transform; } }

    public Coordinate getNewDirection(Coordinate currentDirection)
    {
        return currentDirection;
    }

    public bool causesMovementChange()
    {
        return false;
    }

    public bool stopsMovement()
    {
        return false;
    }

    public bool canBeOnTile()
    {
        return true;
    }

}
