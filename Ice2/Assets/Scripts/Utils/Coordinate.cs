﻿using UnityEngine;
using System.Collections;
using System;

public class Coordinate
{

    public int x;
    public int y;

    public Coordinate(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static implicit operator Vector2(Coordinate coord)
    {
        return new Vector2(coord.x, coord.y);
    }

    public static implicit operator Coordinate(Vector2 vector)
    {
        return new Coordinate(Convert.ToInt16(vector.x), Convert.ToInt16(vector.y));
    }

    public static Coordinate operator +(Coordinate first, Coordinate second)
    {
        return new Coordinate(first.x + second.x, first.y + second.y);
    }

    public static Coordinate operator -(Coordinate first, Coordinate second)
    {
        return new Coordinate(first.x - second.x, first.y - second.y);
    }

}
