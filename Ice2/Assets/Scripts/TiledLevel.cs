﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TiledLevel : MonoBehaviour
{

    private Transform bottomLeftTransform;
    private Transform topRightTransform;
    private float tileWorldSize;
    private int gridHeight;
    private int gridWidth;
    private ITile[,] tileMap;
    private Player player;
    public Transform gridContainer;

    public Player Player
    {
        get { return player; }
    }

    private Transform goal;

    // Use this for initialization
    void Awake ()
	{
        //put all tiles into a flat array
        gridContainer = this.transform;
	    List<Transform> flatGrid = getFlattenedArrayChildren(gridContainer);

        //Find the bottom left tile
        bottomLeftTransform = findBottomLeftTransform(flatGrid);

        //Find top right tile
        topRightTransform = findTopRightTransform(flatGrid);

        //Get width (and infer height) of tile - all tiles must be this shape
        tileWorldSize = bottomLeftTransform.GetComponent<Renderer>().bounds.size.x;

        //find the size of the whole grid
        //the + 1 is needed, because the division doesn't take into account the edges of the tile either side of the origin of the corner tiles
        gridHeight = Convert.ToInt16((topRightTransform.position.y - bottomLeftTransform.position.y)/tileWorldSize) + 1;
        gridWidth = Convert.ToInt16((topRightTransform.position.x - bottomLeftTransform.position.x) / tileWorldSize) + 1;

        //go through flat array and put in the right slots in the grid based on location, apart from the player. Keep them separate
        tileMap = new ITile[gridWidth,gridHeight];
        int i = 0;
        foreach (Transform tile in flatGrid)
        {
            Vector2 tileMapPosition = (tile.position - bottomLeftTransform.position)/tileWorldSize;
            int x = Convert.ToInt16(tileMapPosition.x);
            int y = Convert.ToInt16(tileMapPosition.y);
            ITile currentWorkingTile = tileMap[x, y];
            if (tile.tag.Equals("Player"))
            {
                player = tile.GetComponent<Player>();
                player.gridLocation = new Coordinate(x, y);
                continue;
            }
            if (tile.tag.Equals("Goal"))
            {
                goal = tile;
            }

            if (currentWorkingTile != null)
            {
                throw new Exception("Two tiles on the same spot at coordinates (" + x + ", " + y + "), position: " + tile.position);
            }
            tileMap[x, y] = tile.GetComponent<ITile>();
            tileMap[x, y].gridLocation = new Coordinate(x, y);
            i++;
        }
    }

    //This gets flattens the list of all children. Does not return itself as an item in the list
    private List<Transform> getFlattenedArrayChildren(Transform node)
    {
        List<Transform> flattenedList = new List<Transform>();

        
        foreach (Transform child in node)
        {
            List<Transform> childList = getFlattenedArrayChildren(child);
            if (childList.Count == 0)
            {
                if(child.GetComponent<LevelObject>() != null) { flattenedList.Add(child);}
            }
            else
            {
                flattenedList.AddRange(childList);
            }
        }

        return flattenedList;
    }

    private Transform findBottomLeftTransform(List<Transform> transforms)
    {
        Transform currentBottomLeft;
        if (transforms.Count != 0)
        {
            currentBottomLeft = transforms[0];
        }
        else
        {
            return null;
        }

        foreach (Transform transform in transforms)
        {
            if (transform.position.x <= currentBottomLeft.position.x)
            {
                if (transform.position.y <= currentBottomLeft.position.y)
                {
                    currentBottomLeft = transform;
                }
            }
        }

        return currentBottomLeft;

    }

    private Transform findTopRightTransform(List<Transform> transforms)
    {
        Transform currentTopRight;
        if (transforms.Count != 0)
        {
            currentTopRight = transforms[0];
        }
        else
        {
            return null;
        }

        foreach (Transform transform in transforms)
        {
            if (transform.position.x >= currentTopRight.position.x)
            {
                if (transform.position.y >= currentTopRight.position.y)
                {
                    currentTopRight = transform;
                }
            }
        }

        return currentTopRight;

    }

    public ITile getNextMovementChangeLocation(ITile[,] map, Coordinate currentLocation, Coordinate direction)
    {
        if (direction.x == 0 && direction.y == 0)
        {
            Debug.Log("bad direc");
            return null;
        }
        Coordinate currentMapLocation = currentLocation;
        Coordinate nextMapLocation = currentMapLocation;
        bool locationFound = false;

        while (!locationFound)
        {
            nextMapLocation = nextMapLocation + direction;
            if (!isLocationInMap(map, nextMapLocation))
            {
                locationFound = true;
                nextMapLocation = nextMapLocation - direction;
                return map[nextMapLocation.x, nextMapLocation.y];
            }

            if (map[nextMapLocation.x, nextMapLocation.y].causesMovementChange())
            {
                locationFound = true;
                player.directionOfMovement = map[nextMapLocation.x, nextMapLocation.y].getNewDirection(player.directionOfMovement);
                if (map[nextMapLocation.x, nextMapLocation.y].stopsMovement())
                {
                    if (!map[nextMapLocation.x, nextMapLocation.y].canBeOnTile())
                    {
                        nextMapLocation = nextMapLocation - direction;
                    }

                }
                return map[nextMapLocation.x, nextMapLocation.y];
            }
        }

        //we'll never reach here, because we'll leave the map and return the edge
        return map[currentMapLocation.x, currentMapLocation.y];
    }

    public ITile getNextMovementChangeLocation(Coordinate direction)
    {
        return getNextMovementChangeLocation(tileMap, player.gridLocation, direction);
    }

    public static bool isLocationInMap(ITile[,] map, Coordinate location)
    {
        int width = map.GetLength(0);
        int height = map.GetLength(1);
        return !(location.x < 0 || location.x >= width || location.y < 0 || location.y >= height);
    }

    public ITile getTileAtGivenCoordinates(Coordinate coordinate)
    {
        return tileMap[coordinate.x, coordinate.y];
    }

    public bool hasPlayerStopped()
    {
        ITile currentTile = tileMap[player.gridLocation.x, player.gridLocation.y];
        Coordinate nextLocation = player.gridLocation + player.directionOfMovement;
        
        return currentTile.stopsMovement() || (isLocationInMap(tileMap, nextLocation) && tileMap[nextLocation.x, nextLocation.y].stopsMovement());
    }

    // Update is called once per frame
    void Update () {
	
	}
}
