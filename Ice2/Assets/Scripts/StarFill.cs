﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarFill : MonoBehaviour
{

    public Animator star1;
    public Animator star2;
    public Animator star3;
    private List<Animator> orderedStarList;

    void Awake()
    {
        orderedStarList = new List<Animator>() { star1, star2, star3};
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void fillAppropriateNumberOfStars(int movesTaken, int bestNumOfMoves, float twoStarPercentage)
    {
        int numOfStars = 1;
        if (movesTaken <= bestNumOfMoves)
        {
            numOfStars = 3;
        } else if (movesTaken <= (bestNumOfMoves + (bestNumOfMoves*twoStarPercentage)))
        {
            numOfStars = 2;
        }
        StartCoroutine(DoAnimation(0.7f, numOfStars));  
    }

    IEnumerator DoAnimation(float timeBetweenAnimations, int numOfStars)
    {
        while (true)
        {
            Debug.Log("In the while loop");
            for (int i = 0; i < numOfStars; i++)
            {
                orderedStarList[i].SetTrigger("fill");
                yield return new WaitForSeconds(timeBetweenAnimations);
            }
        }
    }

}
