﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{

    public TiledLevel levelContainer;
    private bool isMovingBetweenTiles = false;
    private bool isStillSliding = false;
    private Vector2 startPosition;
    private float t;
    private Vector2 endPosition;
    public float moveSpeed = 3f;
    private Vector2 input;
    public float gridSize = 0.4f;
    private Player player;
    private float distance;
    private ITile locationToMoveTo;
    private bool levelComplete = false;

    public GameObject successMenu;

    public int targetNumOfMoves;
    public float percentageOfTargetForTwoStars = 0.5f;

    private int movesSoFar = 0;
    public Text numberOfMovesText;
    public bool canControlMovement = true;


    // Use this for initialization
    void Start ()
    {
        player = levelContainer.Player;
    }
	
	// Update is called once per frame
	void Update () {

	    if (!isMovingBetweenTiles && !levelComplete && canControlMovement)
	    {
            //SHITTY CODE - IMPROVE THIS WITH ENUMS OR SOMETHING M8
            if (Input.GetKeyDown("w"))
	        {
                player.directionOfMovement = new Coordinate(0, 1);
	            StartCoroutine(move(player));
	        }
            else if (Input.GetKeyDown("s"))
            {
                player.directionOfMovement = new Coordinate(0, -1);
                StartCoroutine(move(player));
            }
            else if (Input.GetKeyDown("a"))
            {
                player.directionOfMovement = new Coordinate(-1, 0);
                StartCoroutine(move(player));
            }
            else if (Input.GetKeyDown("d"))
            {
                player.directionOfMovement = new Coordinate(1, 0);
                StartCoroutine(move(player));
            }
        }
	    if (!isMovingBetweenTiles && !canControlMovement)
	    {
            StartCoroutine(move(player));
        }
    }

    public IEnumerator move(Player player)
    {
        //work out the final location of the next tile which will cause something to happen
        //i.e. hit a wall, touch a portal / direction change, go in a hole

        locationToMoveTo = levelContainer.getNextMovementChangeLocation(player.directionOfMovement);
        if (!(player.gridLocation.x == locationToMoveTo.gridLocation.x &&
              player.gridLocation.y == locationToMoveTo.gridLocation.y))
        {
            movesSoFar++;
        }

        isMovingBetweenTiles = true;
        canControlMovement = false;
        startPosition = player.transform.position;
        t = 0;

        endPosition = locationToMoveTo.getTransform.position;
        
        float xDiff = (endPosition - startPosition).x;
        distance = xDiff.Equals(0) ? (endPosition - startPosition).y : xDiff;
        distance = Math.Abs(distance);

        while (t < 1f)
        {
            t += Time.deltaTime * (moveSpeed / distance);
            player.transform.position = Vector3.Lerp(startPosition, endPosition, t);
            yield return null;
        }

        isMovingBetweenTiles = false;
        player.gridLocation = locationToMoveTo.gridLocation;
        if (levelContainer.hasPlayerStopped())
        {
            canControlMovement = true;
        }
        if (levelContainer.getTileAtGivenCoordinates(player.gridLocation).getTransform.tag == "Goal")
        {
            levelComplete = true;
            levelCompleted();
        }
        yield return 0;
    }

    private void levelCompleted()
    {
        numberOfMovesText.text = "Number Of Moves:\n" + movesSoFar;
        successMenu.SetActive(true);
        StartCoroutine(waitSecondsThenFillStars(0.5f));
    }

    IEnumerator waitSecondsThenFillStars(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Debug.Log(successMenu);
        Debug.Log(successMenu.GetComponent<StarFill>());
        successMenu.GetComponent<StarFill>().fillAppropriateNumberOfStars(movesSoFar, targetNumOfMoves, percentageOfTargetForTwoStars);
    }
}
