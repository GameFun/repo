﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public static class Globals
    {
        public static StartingState STARTING_STATE = StartingState.Main;
    }

    public enum StartingState
    {
        Main = 1, Levels = 2
    }
}