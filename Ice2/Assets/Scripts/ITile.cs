﻿using UnityEngine;

public interface ITile 
{
    Transform getTransform { get; }
    Coordinate gridLocation { get; set; }
    bool causesMovementChange();
    bool stopsMovement();
    bool canBeOnTile();
    Coordinate getNewDirection(Coordinate currentDirection);

}