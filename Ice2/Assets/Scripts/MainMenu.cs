﻿using Assets.Scripts;
using UnityEngine;

public class MainMenu : MonoBehaviour
{


    public GameObject mainMenu;
    public GameObject levelSelect;


    // Use this for initialization
    void Start()
    {
        if (Globals.STARTING_STATE.Equals(StartingState.Main))
        {
            mainMenu.GetComponent<Animator>().SetTrigger("MoveRight");
            levelSelect.GetComponent<Animator>().SetTrigger("MoveRight");
        }
        else
        {
            mainMenu.GetComponent<Animator>().SetTrigger("MoveLeft");
            levelSelect.GetComponent<Animator>().SetTrigger("MoveLeft");
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
