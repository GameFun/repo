﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InternalTileMap
{

    private Dictionary<Vector2, GameObject> tileMap = new Dictionary<Vector2, GameObject>();

    public void generateMap(GameObject mapContainer)
    {
        List<GameObject> flattenedObjects = getFlattenedArrayChildren(mapContainer);
        foreach (GameObject tile in flattenedObjects)
        {
            tileMap[tile.transform.position] = tile;
        }
        
    }

    private List<GameObject> getFlattenedArrayChildren(GameObject node)
    {
        List<GameObject> flattenedList = new List<GameObject>();


        foreach (Transform child in node.transform)
        {
            List<GameObject> childList = getFlattenedArrayChildren(child.gameObject);
            if (childList.Count == 0)
            {
                if (child.GetComponent<LevelObject>() != null) { flattenedList.Add(child.gameObject); }
            }
            else
            {
                flattenedList.AddRange(childList);
            }
        }

        return flattenedList;
    }

    public GameObject addNewTileAndGetOldTileIfExists(GameObject newTile)
    {
        GameObject tileToReturn = getTileAtPositionIfExists(newTile.transform.position);
        tileMap[newTile.transform.position] = newTile;
        return tileToReturn;
    }

    public GameObject getTileAtPositionIfExists(Vector2 position)
    {
        GameObject tileToReturn = null;
        if (tileMap.ContainsKey(position))
        {
            tileToReturn = tileMap[position];
        }
        return tileToReturn;
    }
}
