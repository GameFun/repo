﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
public class TileWindow : EditorWindow
{
    
    private TileBrush brush;

    public Vector2 scrollPos = Vector2.zero;
    public int viewportEdgeOffset = 42;
    public int gapSize = 10;
    public int size = 64;
    public int itemsPerRow = 0;
    Vector2? currentSelection = null;
    private int numOfTiles = 0;
    private GameObject currentSelectedTile;
    private GameObject currentPaintingTile;
    private GameObject tileAnchor;
    private Vector2 mousePosition;
    private InternalTileMap internalTileMap = new InternalTileMap();
    private float tileSize = 0.4f;

    [MenuItem("Window/Tile Picker")]
    public static void OpenTileWindow()
    {
        var window = EditorWindow.GetWindow(typeof(TileWindow));
        var title = new GUIContent();
        title.text = "Tile Picker";
        window.titleContent = title;
    }

    void OnEnable()
    {
        tileAnchor = GameObject.Find("TileAnchor");
        SceneView.onSceneGUIDelegate += OnSceneGUI;
        internalTileMap.generateMap(tileAnchor);
    }

    void OnDisable()
    {
        DestroyBrush();
    }

    void OnGUI()
    {
        string[] prefabFiles = Directory.GetFiles("Assets/Prefabs/Tiles/", "*.prefab", SearchOption.TopDirectoryOnly);
        GameObject[] loadedPrefab = new GameObject[prefabFiles.Length];
        numOfTiles = loadedPrefab.Length;
        int i = 0;
        int maxWidth = 0;
        int maxHeight = 0;
        
        
        int widthSoFar = gapSize;
        int heightSoFar = gapSize;

        foreach (string assetPath in prefabFiles)
        {
            loadedPrefab[i] = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
            maxWidth += size + gapSize;
            i++;
        }

        var viewPort = new Rect(0, 0, position.width, position.height);
        itemsPerRow = Convert.ToInt32(Math.Floor((position.width - gapSize) / (size + gapSize)));
        if (itemsPerRow == 0)
        {
            itemsPerRow++;
        }
        int numOfRows = loadedPrefab.Length / itemsPerRow;
        if (loadedPrefab.Length%itemsPerRow != 0)
        {
            numOfRows++;
        }

        var contentSize = new Rect(0, 0, position.width - 15, numOfRows * (size + gapSize));
        scrollPos = GUI.BeginScrollView(viewPort, scrollPos, contentSize);

        int amountInThisRow = 0;
        foreach (GameObject tilePrefab in loadedPrefab)
        {
            Texture2D currentTexture2D = tilePrefab.GetComponent<SpriteRenderer>().sprite.texture;
            GUI.DrawTexture(new Rect(widthSoFar, heightSoFar + 0, size, size), currentTexture2D);
            widthSoFar += size + gapSize;
            amountInThisRow++;
            i++;
            if (amountInThisRow == itemsPerRow)
            {
                amountInThisRow = 0;
                heightSoFar += size + gapSize;
                widthSoFar = gapSize;
            }
        }

        var boxTex = new Texture2D(1, 1);
        boxTex.SetPixel(0,0, new Color(0, 0.5f, 1f, 0.4f));
        boxTex.Apply();

        var style = new GUIStyle(GUI.skin.customStyles[0]);
        style.normal.background = boxTex;


        var cEvent = Event.current;
        Vector2 mousePos = new Vector2(cEvent.mousePosition.x, cEvent.mousePosition.y);
        if (currentSelection.HasValue)
        {
            GUI.Box(new Rect(currentSelection.Value, new Vector2(size, size)), "", style);
        }

        if (cEvent.type == EventType.mouseDown && cEvent.button == 0)
        {
            
            int? arrayPos = getTilePositionFromPoint(mousePos);
            if (arrayPos.HasValue)
            {
                currentSelection = getEditorPositionFromArrayPosition(arrayPos.Value);
                currentSelectedTile = loadedPrefab[arrayPos.Value];
                Debug.Log("Current tile : " + currentSelectedTile);
                createBrush();
            }
            else
            {
                DestroyBrush();
            }
            Repaint();
        }

        GUI.EndScrollView();
    }

    private int? getTilePositionFromPoint(Vector2 mousePosition)
    {
        float xMinusGap = mousePosition.x - gapSize;
        float yMinusGap = mousePosition.y - gapSize;

        int row = Convert.ToInt32(Math.Floor(yMinusGap/(size + gapSize)));
        float amountPastRow = yMinusGap % (size + gapSize);
        if (amountPastRow > size)
        {
            currentSelection = null;
            return null;
        }

        int column = Convert.ToInt32(Math.Floor(xMinusGap / (size + gapSize)));
        float amountPastColumn = xMinusGap % (size + gapSize);
        if (amountPastColumn > size)
        {
            currentSelection = null;
            return null;
        }

        if (column == itemsPerRow && amountPastColumn != 0)
        {
            currentSelection = null;
            return null;
        }

        if (((row*itemsPerRow) + column) >= numOfTiles)
        {
            currentSelection = null;
            return null;
        }
        return (row*itemsPerRow) + column;
    }

    private Vector2 getEditorPositionFromArrayPosition(int arrayPos)
    {
        int row = arrayPos/itemsPerRow;
        int column = arrayPos%itemsPerRow;
        Vector2 location = new Vector2(gapSize + (column * (size + gapSize)), gapSize + (row * (size + gapSize)));
        return location;
    }

    private void createBrush()
    {
        if (currentSelectedTile != null)
        {
            GameObject createdTile = Instantiate(currentSelectedTile);
            createdTile.transform.position = new Vector3(0, 0, -5);
            createdTile.transform.parent = tileAnchor.transform;
            if (currentPaintingTile != null)
            {
                DestroyImmediate(currentPaintingTile);
            }
            currentPaintingTile = createdTile;

        }
    }

    public void OnSceneGUI(SceneView sceneView)
    {
        updateHitPosition();
        moveBrush();
        var cEvent = Event.current;
        if (cEvent.shift)
        {
            if (currentPaintingTile != null)
            {
                GameObject oldTile = internalTileMap.addNewTileAndGetOldTileIfExists(currentPaintingTile);
                DestroyImmediate(oldTile);
                currentPaintingTile = null;
                createBrush();
            }
        }
        if (cEvent.alt)
        {
            GameObject oldTile = internalTileMap.getTileAtPositionIfExists(getLockedToGridMousePosition());
            DestroyImmediate(oldTile);
        }

    }


    void DestroyBrush()
    {
        if (currentPaintingTile != null)
        {
            DestroyImmediate(currentPaintingTile);
        }
    }

    private void updateHitPosition()
    {
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        // create a plane at 0,0,0 whose normal points to +Y:
        Plane hPlane = new Plane(Vector3.forward, Vector3.zero);
        // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
        float distance = 0;
        // if the ray hits the plane...
        if (hPlane.Raycast(ray, out distance))
        {
            mousePosition = ray.GetPoint(distance);
        }
    }

    private void moveBrush()
    {
        if (currentPaintingTile != null)
        {
            Debug.Log("Current mouse position : " + mousePosition);
            Vector3 newMousePosition = getLockedToGridMousePosition();
            Debug.Log("New mouse position : " + newMousePosition);

            Vector2 tileAnchorOffset = new Vector2(tileAnchor.transform.position.x%tileSize,
                tileAnchor.transform.position.y%tileSize);

            currentPaintingTile.transform.position = new Vector3(newMousePosition.x, newMousePosition.y, -5);
            Debug.Log("transform's position = " + currentPaintingTile.transform.position);
        }
    }


    private Vector3 getLockedToGridMousePosition()
    {
        return new Vector3(tileSize*Mathf.Round(mousePosition.x/tileSize),
            tileSize*Mathf.Round(mousePosition.y/tileSize), -5);
    }
}
