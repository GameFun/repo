﻿using UnityEngine;
using System.Collections;

public class TileBrush : MonoBehaviour {

    public Vector2 brushSize = Vector2.zero;
    public int tileId = 0;
    public SpriteRenderer renderer2D;	
	
    void onDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, brushSize);
    }

    public void UpdateBrush(Sprite tile)
    {
        renderer2D.sprite = tile;
    }
}
