﻿using UnityEngine;
using System.Collections;

public class MovementFromButtonController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void clearMoveRightTrigger()
    {
        Animator animator = GetComponent<Animator>();
        animator.ResetTrigger("MoveRight");
    }

    public void clearMoveLeftTrigger()
    {
        Animator animator = GetComponent<Animator>();
        animator.ResetTrigger("MoveLeft");
    }
}
