﻿using UnityEngine;
using System.Collections;

public class ChangeableObject : MonoBehaviour
{

    public bool startingIsVisible = false;
    public bool currentIsVisible = false;
    private Animator animator;
    private bool collidingWithBall = false;
    
    // Use this for initialization
    void Start ()
	{
	    currentIsVisible = startingIsVisible;
        animator = GetComponent<Animator>();


	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKey("space"))
	    {
            if (currentIsVisible || (!currentIsVisible && !collidingWithBall))
            {
                currentIsVisible = !startingIsVisible;
                GetComponent<Collider2D>().isTrigger = startingIsVisible;
            }   
        }
	    else
	    {
	        if (currentIsVisible || (!currentIsVisible && !collidingWithBall))
	        {
	            currentIsVisible = startingIsVisible;
	            GetComponent<Collider2D>().isTrigger = !startingIsVisible;
	        }
	    }

        animator.SetBool("currentIsVisible", currentIsVisible);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("InnerPlayer"))
        {
            collidingWithBall = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("InnerPlayer"))
        {
            collidingWithBall = false;
        }
    }
}
