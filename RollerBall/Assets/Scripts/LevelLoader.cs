﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void reloadCurrentLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void loadNextLevelOrLoop()
    {
        Debug.Log("Current Scene = " + SceneManager.GetActiveScene().buildIndex);
        int nextScene = SceneManager.GetActiveScene().buildIndex + 1;
        Debug.Log("Next scene = " + nextScene);
        Debug.Log("Total num of scenes = " + SceneManager.sceneCountInBuildSettings);
        if (nextScene < SceneManager.sceneCountInBuildSettings)
        {
            Debug.Log(nextScene);
            SceneManager.LoadScene(nextScene);
        }
        else
        {
            SceneManager.LoadScene(1); //this is starting at 1, because 0 is the main menu
        }
        
    }
}
