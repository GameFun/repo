﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Level : MonoBehaviour
{

    public GameObject ball;

    public int currentLevel;

    // Use this for initialization
    void Start ()
    {
        ball.GetComponent<Rigidbody2D>().isKinematic = true;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void makeBallMoveAtStart()
    {
        ball.GetComponent<Rigidbody2D>().isKinematic = false;
    }
}
