﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEngine.SceneManagement;

public class LoadMainMenu : MonoBehaviour {

    public void loadMainMenuLevels()
    {
        Globals.STARTING_STATE = StartingState.Levels;
        SceneManager.LoadScene(0);
    }
}
