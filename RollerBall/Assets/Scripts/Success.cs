﻿using UnityEngine;
using System.Collections;

public class Success : MonoBehaviour {

    public GameObject successMenu;
    public GameObject player;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //.SetActive(true);
            
            //TODO
            //move the ball to the center of the circle with a coroutine probably
            // add some pumping music when victoru is obtained
            successMenu.SetActive(true);
            player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
        }
    }
}
