﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour
{
    //Will put the Success menu here
    ///public GameObject replayMenu;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("You win");
            //replayMenu.SetActive(true);
        }
    }
}
