﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Graphing
{
    public class Node : IEquatable<Node>
    {
        public int x;
        public int y;

        public float worldX;
        public float worldY;

        public DirtTile tile;

        public Node(int x, int y, float worldX, float worldY, DirtTile tile)
        {
            this.x = x;
            this.y = y;
            this.worldX = worldX;
            this.worldY = worldY;
            this.tile = tile;
        }

        public bool Equals(Node n)
        {
            return (this.x == n.x && this.y == n.y);
        }
    }
}
