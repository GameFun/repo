﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Graphing
{
    public class AStarNode
    {
        public AStarNode parent;
        public AStarNode child;
        public Node data;
        public int cost;

        public AStarNode(AStarNode parent, Node data, int cost)
        {
            this.parent = parent;
            this.data = data;
            this.cost = cost;
        }
    }
}
