﻿using Assets.Scripts.Graphing;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGrid : MonoBehaviour {


    public int columns = 25;
    public int rowsToFill = 10;
    private int maxRows;
    public GameObject tile;
    public DirtTile[,] tiles;
    private float tileWidth;
    public float damageAmount = 50;
    private List<DiggerAI> diggers;
    private Transform boardHolder;
    public float gravityScale = 70;
    private bool hasBeenFirstClick = false;
    private float timerForDoubleClick;
    public float doubleClickDelay = 0.3f; //this is how long in seconds to allow for a double click

    // Use this for initialization
    void Start () {
        string coolMap =        "********************************\r\n" +
                                "*##############################*\r\n" +                                "*#                            #*\r\n" +                                "*#                            #*\r\n" +                                "*##  #  #  #  #  #  #  #  #  ##*\r\n" +                                "*###                        ###*\r\n" +                                "*####       ## ## ##       ####*\r\n" +                                "*##### ###            ### #####*\r\n" +                                "*#        #          #        #*\r\n" +                                "*#         #        #         #*\r\n" +
                                "*# #  #  #  #  ##  #  #  #  # #*\r\n" +
                                "*#            #  #            #*\r\n" +
                                "*#  #  #  #  #    #  #  #  #  #*\r\n" +
                                "*#          #      #          #*\r\n" +
                                "*##  #  #  #        #  #  #  ##*\r\n" +
                                "*#        #          #        #*\r\n" +
                                "*# #  #  #  #  ##  #  #  #  # #*\r\n" +
                                "*#      #     #  #     #      #*\r\n" +
                                "*#     #     #    #     #     #*\r\n" +
                                "*#    #  #  #      #  #  #    #*\r\n" +
                                "*##  #  ###          ###  #  ##*\r\n" +
                                "*###   #####        #####   ###*\r\n" +
                                "*#### #######      ####### ####*\r\n" +
                                "*##############################*\r\n" +
                                "********************************";
        setCameraBottomLeftAtOrigin();
        generateTilesToFillCameraWidth(columns, rowsToFill, tile, coolMap);
        findAllDiggers();
    }

    private void findAllDiggers()
    {
        diggers = new List<DiggerAI>();
        GameObject[] diggersArray = GameObject.FindGameObjectsWithTag("Digger");
        foreach(GameObject digger in diggersArray){
            this.diggers.Add(digger.GetComponent<DiggerAI>());
            digger.GetComponent<DiggerAI>().tileGrid = this;
            digger.transform.localScale = new Vector3(tileWidth / 4, tileWidth / 4, 1);
            digger.GetComponent<Rigidbody2D>().gravityScale = calculateGravityForDigger();
        }
    }

    private float calculateGravityForDigger()
    {
        return gravityScale / columns;
    }

    private void setCameraBottomLeftAtOrigin()
    {
        float aspectRatio = Camera.main.aspect;
        float camSize = Camera.main.orthographicSize;
        float correctPositionX = aspectRatio * camSize;
        Camera.main.transform.position = new Vector3(correctPositionX, camSize, -10);
    }

    private void generateTilesToFillCameraWidth(int numOfColumns, int numOfRows, GameObject tile, String gridToCreate)
    {
        //convert tile to 1 unit width and height
        float unitWidth = tile.GetComponent<SpriteRenderer>().sprite.bounds.size.x; //unit width of the object
        float oneUnitMult = 1 / unitWidth;
        tile.transform.localScale = new Vector3(oneUnitMult, oneUnitMult, 1);
        boardHolder = new GameObject("board").transform;
        if (gridToCreate == null || gridToCreate.Length == 0)
        {
            //width that the camera sees in units
            float visibleWidth = (float)(Camera.main.orthographicSize * 2.0 * Screen.width / Screen.height) / numOfColumns;
            tileWidth = visibleWidth;

            float unitsHeightOfScreen = Camera.main.orthographicSize * 2;
            int totalRowsToFillScreen = Mathf.CeilToInt(unitsHeightOfScreen / tileWidth);
            maxRows = totalRowsToFillScreen;

            tiles = new DirtTile[columns, totalRowsToFillScreen];

            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < totalRowsToFillScreen; y++)
                {
                    if (y < numOfRows)
                    {
                        createAndAttachTile(x, y, tiles);
                    }
                    else
                    {
                        tiles[x, y] = null;
                    }

                }
            }
        } else
        {
            string[] lines = gridToCreate.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            int stringRows = lines.Length - 2;
            int stringColumns = lines[0].Length - 2;
            //width that the camera sees in units
            float visibleWidth = (float)(Camera.main.orthographicSize * 2.0 * Screen.width / Screen.height) / stringColumns;
            this.tileWidth = visibleWidth;
            tiles = createGridFromString(gridToCreate);
        }
    }

    private void createAndAttachTile(int x, int y, DirtTile[,] tilesGrid)
    {
        GameObject newTile = Instantiate(tile, new Vector3(x * tileWidth + (tileWidth / 2), y * tileWidth + (tileWidth / 2), 0), Quaternion.identity);
        newTile.transform.localScale *= tileWidth;
        newTile.transform.SetParent(boardHolder);
        tilesGrid[x, y] = newTile.GetComponent<DirtTile>();
        newTile.GetComponent<DirtTile>().x = x;
        newTile.GetComponent<DirtTile>().y = y;
        newTile.transform.parent = boardHolder;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButton(0))
        {
            damageTile();
        }
        if (Input.GetMouseButton(1))
        {
            attachTileAtMouse();   
        }
        checkForDoubleAndClickMoveDigger();
        if (Input.GetKeyDown("space"))
        {
            String gridString = convertGridToString();
            Debug.Log(gridString);
        }
    }

    private void checkForDoubleAndClickMoveDigger()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!hasBeenFirstClick) // first click no previous clicks
            {
                hasBeenFirstClick = true;
                timerForDoubleClick = Time.time; // save the current time
            }
            else
            {
                hasBeenFirstClick = false; // found a double click, now reset
                moveDigger();
            }
        }
        if (hasBeenFirstClick)
        {
            // if the time now is delay seconds more than when the first click started. 
            if ((Time.time - timerForDoubleClick) > doubleClickDelay)
            {
                hasBeenFirstClick = false;
            }
        }
    }

    void FixedUpdate()
    {
        
    }

    private void attachTileAtMouse()
    {
        Vector2Int tileCoordinate = getTileCoordinateFromMouse();
        foreach(DiggerAI digger in diggers)
        {
            Vector2Int diggerLocation = getTileCoordinate(digger.transform.position.x, digger.transform.position.y);
            if(tileCoordinate == diggerLocation)
            {
                return;
            }
        }
        DirtTile tile = getTileAtLocation(tileCoordinate);
        if (tile == null)
        {
            createAndAttachTile(tileCoordinate.x, tileCoordinate.y, tiles);
            tellAllDiggersTileHasBeenUpdated(tileCoordinate, true);
        }

    }

    private void tellAllDiggersTileHasBeenUpdated(Vector2Int tileCoordinate, bool tileAdded)
    {
        foreach (DiggerAI digger in diggers)
        {
            digger.gridUpdated(tileCoordinate, tileAdded);
        }
    }

    private void damageTile()
    {
        Vector2Int tileCoordinate = getTileCoordinateFromMouse();
        DirtTile tile = getTileAtLocation(tileCoordinate);
        
        if (tile != null)
        {
            if (tile.damage(damageAmount) <= 0)
            {
                tiles[tileCoordinate.x, tileCoordinate.y] = null;
                Destroy(tile.gameObject);
                tellAllDiggersTileHasBeenUpdated(tileCoordinate, false);
            }
        }
    }

    private void moveDigger()
    {
        Vector2Int tileCoordinate = getTileCoordinateFromMouse();
        DirtTile tile = getTileAtLocation(tileCoordinate);

        if (tile == null)
        {
            Vector3 diggerLoc = diggers[0].transform.position;
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            StartCoroutine(diggers[0].followPath(mousePos, false));
        }
    }

    private DirtTile getTileAtLocation(Vector2Int loc)
    {
        DirtTile currentTile = null;
        if (!(loc.x < 0 || loc.x >= columns || loc.y < 0 || loc.y >= maxRows))
        {
            currentTile = tiles[loc.x, loc.y];
        }
        return currentTile;
    }

    public AStarNode getPathToTile(float xCurrent, float yCurrent, float xTarget, float yTarget)
    {
        Vector2Int targetTileLoc = getTileCoordinate(xTarget, yTarget);
        DirtTile targetTile = getTileAtLocation(targetTileLoc);
        if (targetTile == null)
        {

            Vector2Int currentCoordinate = getTileCoordinate(xCurrent, yCurrent);
            Vector3 newLocWorld = getWorldLocation(targetTileLoc.x, targetTileLoc.y);

            AStarNode pathToPosition = AStar(new Node(currentCoordinate.x, currentCoordinate.y, xCurrent, yCurrent, null), new Node(targetTileLoc.x, targetTileLoc.y, newLocWorld.x, newLocWorld.y, null));
            if (pathToPosition != null)
            {
                return pathToPosition;
            }
            else
            {
            }
        }
        return null;
    }

    public Vector2Int getTileCoordinate(float x, float y)
    {
        int xTile = (int)(x / tileWidth);
        int yTile = (int)(y / tileWidth);
        return new Vector2Int(xTile, yTile);
    }

    private Vector2Int getTileCoordinateFromMouse()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return getTileCoordinate(pos.x, pos.y);
    }


    private DirtTile getTopLeftTile()
    {
        for(int i = 0; i < columns; i++)
        {
            for(int j = maxRows - 1; j >= 0; j--)
            {
                if(tiles[i, j] != null)
                {
                    return tiles[i, j];
                }
            }
        }
        return null;
    }

    private AStarNode AStar(Node start, Node end)
    {
        List<AStarNode> openList = new List<AStarNode>();
        List<AStarNode> closedList = new List<AStarNode>();

        if (start.Equals(end))
        {
            return new AStarNode(null, start, 0);
        }

        openList.Add(new AStarNode(null, start, 0));
        while(openList.Count > 0)
        {
            //find AStarNode with lowest cost
            AStarNode currentNode = getLowestCostNodeAndRemoveFromList(openList);

            if (currentNode.data.Equals(end))
            {
                return reverseNodeGraph(currentNode);
            }

            //find neighbours and calculate costs (taking tile jumps into account)
            List<Node> neighbours = getPossibleNeighboursOfCurrentNode(currentNode.data);

            //calculate cost for each neighbour, and set their parent to be the currentNode
            foreach(Node neighbour in neighbours)
            {
                AStarNode aStarNode = new AStarNode(currentNode, neighbour, 0);

                int manhattanCostToEnd = Mathf.Abs(neighbour.x - end.x) + Mathf.Abs(neighbour.y - end.y);
                int manhattanCostToCurrent = Mathf.Abs(neighbour.x - currentNode.data.x) + Mathf.Abs(currentNode.data.y - end.y);
                if(Mathf.Abs(currentNode.data.x - neighbour.x) > 1)
                {
                    manhattanCostToCurrent += 1;
                }
                aStarNode.cost = manhattanCostToCurrent + manhattanCostToEnd;

                AStarNode existingOpenNode = getSamePositionNodeFromListIfExists(openList, aStarNode);
                if (existingOpenNode != null)
                {
                    if(existingOpenNode.cost <= aStarNode.cost)
                    {
                        continue;
                    }
                }
                AStarNode existingClosedNode = getSamePositionNodeFromListIfExists(closedList, aStarNode);
                if (existingClosedNode != null)
                {
                    if (existingClosedNode.cost <= aStarNode.cost)
                    {
                        continue;
                    }
                }

                //remove the open node if it exists
                //remove the closed node if it exists
                if(existingOpenNode != null)
                {
                    removeSamePositionNode(openList, existingOpenNode);
                }
                if (existingClosedNode != null)
                {
                    removeSamePositionNode(closedList, existingClosedNode);
                }

                openList.Add(aStarNode);
            }
            closedList.Add(currentNode);
        }
        return null;
    }

    private void removeSamePositionNode(List<AStarNode> nodes, AStarNode currentNode)
    { 
        foreach (AStarNode node in nodes)
        {
            if (node.data.Equals(currentNode.data))
            {
                nodes.Remove(node);
                return;
            }
        }
    }

    private AStarNode getSamePositionNodeFromListIfExists(List<AStarNode> nodes, AStarNode currentNode)
    {
        foreach (AStarNode node in nodes)
        {
            if (node.data.Equals(currentNode.data))
            {
                return node;
            }
        }
        return null;
    }

    private AStarNode getLowestCostNodeAndRemoveFromList(List<AStarNode> list)
    {
        int lowest = 0;
        for(int i = 0; i < list.Count; i++){
            if(list[i].cost < list[lowest].cost)
            {
                lowest = i;
            }
        }
        AStarNode lowestNode = list[lowest];
        list.RemoveAt(lowest);
        return lowestNode;
    }

    private List<Node> getPossibleNeighboursOfCurrentNode(Node node)
    {
        List<Node> neighbours = new List<Node>();
        //this all assumes the given node is an empty tile above a walkable tile, else do nothing (e.g. if falling)
        int x = node.x;
        int y = node.y;

        //if left or right are empty and walkable
        Vector3 leftWorld = getWorldLocation(x - 1, y);
        if (isTileWalkable(x - 1, y))
        {
            neighbours.Add(new Node(x - 1, y, leftWorld.x, leftWorld.y, null));
        }

        Vector3 rightWorld = getWorldLocation(x + 1, y);
        if (isTileWalkable(x + 1, y))
        {
            neighbours.Add(new Node(x + 1, y, rightWorld.x, rightWorld.y, null));
        }

        //check if the tiles further to the left and right are empty and walkable, and check
        //that the tiles are free for a jump (tile above current, and both tiles above either left or right, are empty)
        //also check all tiles below
        Node bottomTileLeft = getBottomWalkableTileBelowCurrent(x -2, y);
        if (bottomTileLeft != null)
        {
            if(isTileEmptyAndInTheGrid(x - 2, y) && isTileEmptyAndInTheGrid(x - 1, y) && isTileEmptyAndInTheGrid(x, y + 1) && isTileEmptyAndInTheGrid(x - 1, y + 1) && isTileEmptyAndInTheGrid(x - 2, y + 1)) //check above, next, and next one further
            {
                neighbours.Add(bottomTileLeft);
            }
            
        }

        Node bottomTileRight = getBottomWalkableTileBelowCurrent(x + 2, y);
        if (bottomTileRight != null)
        {
            if (isTileEmptyAndInTheGrid(x + 2, y) && isTileEmptyAndInTheGrid(x + 1, y) && isTileEmptyAndInTheGrid(x, y + 1) && isTileEmptyAndInTheGrid(x + 1, y + 1) && isTileEmptyAndInTheGrid(x + 2, y + 1)) //check above, next, and next one further
            {
                neighbours.Add(bottomTileRight);
            }

        }

        //check if the tiles to left/right are empty, and that the tile one more to the left/right, which is also one up above, is walkable
        //the movement should be able to get to that tile
        //In this image below, the player is O and the destination is X. We should be able to reach there. The empty tiles are .'s
        //..X
        //O.#
        //#..
        //Check that the tile above and the tile above to the left/right are empty, and check
        //the tile above to the left/right + 2 is walkable
        Vector3 leftWorldJump = getWorldLocation(x - 2, y + 1);
        if (isTileWalkable(x - 2, y + 1))
        {
            if (isTileEmptyAndInTheGrid(x, y + 1) && isTileEmptyAndInTheGrid(x - 1, y + 1)) //check above and next
            {
                neighbours.Add(new Node(x -2, y + 1, leftWorldJump.x, leftWorldJump.y, null));
            }
        }

        Vector3 rightWorldJump = getWorldLocation(x + 2, y + 1);
        if (isTileWalkable(x + 2, y + 1))
        {
            if (isTileEmptyAndInTheGrid(x, y + 1) && isTileEmptyAndInTheGrid(x + 1, y + 1)) //check above and next
            {
                neighbours.Add(new Node(x + 2, y + 1, rightWorldJump.x, rightWorldJump.y, null));
            }
        }


        //check if a gap of size 2 can be jumped
        addJumpTwoTiles(x, y, neighbours);

        //if top right or top left are empty, and jumpable (tile above is also empty)
        Vector3 topLeftWorld = getWorldLocation(x - 1, y + 1);
        if (isTileEmptyAndInTheGrid(x, y + 1) && isTileWalkable(x - 1, y + 1))
        {
            neighbours.Add(new Node(x - 1, y + 1, topLeftWorld.x, topLeftWorld.y, null));
        }

        Vector3 topRightWorld = getWorldLocation(x + 1, y + 1);
        if (isTileEmptyAndInTheGrid(x, y + 1) && isTileWalkable(x + 1, y + 1))
        {
            neighbours.Add(new Node(x + 1, y + 1, topRightWorld.x, topRightWorld.y, null));
        }

        //if bottom right or bottom left are empty, the ai could fall to the lowest point. add the lowest point
        if(isTileEmptyAndInTheGrid(x - 1, y) && isTileEmptyAndInTheGrid(x - 1, y - 1))
        {
            Node bottomTile = getBottomWalkableTileBelowCurrent(x - 1, y);
            if(bottomTile != null)
            {
                neighbours.Add(bottomTile);
            }
        }

        if (isTileEmptyAndInTheGrid(x + 1, y) && isTileEmptyAndInTheGrid(x + 1, y - 1))
        {
            Node bottomTile = getBottomWalkableTileBelowCurrent(x + 1, y);
            if (bottomTile != null)
            {
                neighbours.Add(bottomTile);
            }
        }

        return neighbours;
    }

    private void addJumpTwoTiles(int x, int y, List<Node> neighbours)
    {

        //check if the tiles further to the left and right are empty and walkable, and check
        //that the tiles are free for a jump (tile above current, and both tiles above either left or right, are empty)
        //also check all tiles below
        //Below, we will check every 'E' to make sure there is enough space. 7 spaces to check altogether
        /**
         * 
         * EEEE
         * xEEE
         * #  #
         *
         * * */
        Node bottomTileLeft = getBottomWalkableTileBelowCurrent(x - 3, y);
        if (bottomTileLeft != null)
        {
            if (isTileEmptyAndInTheGrid(x - 2, y) && isTileEmptyAndInTheGrid(x - 1, y) && isTileEmptyAndInTheGrid(x - 3, y) && isTileEmptyAndInTheGrid(x, y + 1) && isTileEmptyAndInTheGrid(x - 1, y + 1) 
                && isTileEmptyAndInTheGrid(x - 2, y + 1) && isTileEmptyAndInTheGrid(x - 3, y + 1)) //check above, next, and next one further
            {
                neighbours.Add(bottomTileLeft);
            }

        }

        Node bottomTileRight = getBottomWalkableTileBelowCurrent(x + 3, y);
        if (bottomTileRight != null)
        {
            if (isTileEmptyAndInTheGrid(x + 2, y) && isTileEmptyAndInTheGrid(x + 1, y) && isTileEmptyAndInTheGrid(x + 3, y) && isTileEmptyAndInTheGrid(x, y + 1) && isTileEmptyAndInTheGrid(x + 1, y + 1) 
                && isTileEmptyAndInTheGrid(x + 2, y + 1) && isTileEmptyAndInTheGrid(x + 3, y + 1)) //check above, next, and next one further
            {
                neighbours.Add(bottomTileRight);
            }

        }
    }

    private Node getBottomWalkableTileBelowCurrent(int x, int y)
    {
        while(y >= 0)
        {
            if(isTileWalkable(x, y))
            {
                Vector3 worldLoc = getWorldLocation(x, y);
                return new Node(x, y, worldLoc.x, worldLoc.y, null);
            } else
            {
                y--;
            }
        }
        return null;
    }

    private bool isTileEmptyAndInTheGrid(int x, int y)
    {
        if (x < 0 || y < 0 || x >= columns) //if the tile to check is outside the right/left, or below the bottom, return false
        {
            return false;
        }
        return (y >= maxRows || tiles[x, y] == null);
    }

    //checks if the current tile has a dirt tile underneath it, and is empty
    private bool isTileWalkable(int x, int y)
    {
        if(x < 0 || y < 0 || x >= columns) //if the tile to check is outside the right/left, or below the bottom, return false
        {
            return false;
        }
        if(y >= maxRows || tiles[x, y] == null) // if the tile is above the top (must be air), or the current tile is empty
        {
            if((y - 1) < 0) //if we are checking the very bottom tile
            {
                return false;
            }
            else
            {
                if((y - 1) >= maxRows) //if our current tile is more than one tile above the top (must be jumping?)
                {
                    return false;
                }
                return tiles[x, y - 1] != null;
            }
        }
        return false;
    }

    private Node getTileAtCoordinates(int x, int y)
    {
        if(x >= columns || y >= maxRows || x < 0 || y < 0)
        {
            Vector3 worldLoc = getWorldLocation(x, y);
            return new Node(x, y, worldLoc.x, worldLoc.y, null);
        }

        if (tiles[x, y] == null)
        {
            Vector3 worldLoc = getWorldLocation(x, y);
            return new Node(x, y, worldLoc.x, worldLoc.y, null);
        }
        else
        {
            DirtTile tile = tiles[x, y];
            Node returnTile = new Node(x, y, tile.transform.position.x, tile.transform.position.y, tile);
            return returnTile;
        }
    }

    public Vector3 getWorldLocation(int x, int y)
    {
        float worldX = (x * tileWidth) + (tileWidth / 2);
        float worldY = (y * tileWidth) + (tileWidth / 2);
        Vector3 worldLoc = new Vector3(worldX, worldY, 0);
        return worldLoc;
    }

    public float getTileWidth()
    {
        return tileWidth;
    }

    private AStarNode reverseNodeGraph(AStarNode root)
    {
        Stack<AStarNode> stack = new Stack<AStarNode>();
        stack.Push(root);
        while(root.parent != null)
        {
            root = root.parent;
            stack.Push(root);
        }

        AStarNode newRoot = stack.Pop();
        AStarNode currentItem = newRoot;
        while(stack.Count > 0)
        {
            currentItem.child = stack.Pop();
            currentItem = currentItem.child;
        }
        return newRoot;
        
    }

    public List<Vector2Int> getAllWalkableTilesFromPosition(float startX, float startY)
    {
        HashSet<Vector2Int> visitedSet = new HashSet<Vector2Int>();
        List<Vector2Int> visitedList = new List<Vector2Int>();

        HashSet<Vector2Int> toVisitSet = new HashSet<Vector2Int>();
        Queue<Vector2Int> toVisitQueue = new Queue<Vector2Int>();

        Vector2Int start = getTileCoordinate(startX, startY);
        toVisitSet.Add(start);
        toVisitQueue.Enqueue(start);

        while (toVisitQueue.Count > 0)
        {
            Vector2Int currentVector = toVisitQueue.Dequeue();
            toVisitSet.Remove(currentVector);

            Vector3 worldLoc = getWorldLocation(currentVector.x, currentVector.y);
            Node currentNode = new Node(currentVector.x, currentVector.y, worldLoc.x, worldLoc.y, tiles[currentVector.x, currentVector.y]);
            List<Node> neighbours = getPossibleNeighboursOfCurrentNode(currentNode);

            foreach(Node neighbour in neighbours){
                Vector2Int neighbourVector = new Vector2Int(neighbour.x, neighbour.y);
                if (!visitedSet.Contains(neighbourVector) && !toVisitSet.Contains(neighbourVector))
                {
                    toVisitSet.Add(neighbourVector);
                    toVisitQueue.Enqueue(neighbourVector);
                }
            }

            visitedList.Add(currentVector);
            visitedSet.Add(currentVector);
        }
        return visitedList;
    }

    public void highlightTile(Vector2Int tile)
    {
        if(tiles[tile.x, tile.y] != null)
        {
            tiles[tile.x, tile.y].GetComponent<SpriteRenderer>().color = Color.red;
        }
        
    }

    public void disableHighlightTile(Vector2Int tile)
    {
        if (tiles[tile.x, tile.y] != null)
        {
            tiles[tile.x, tile.y].GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    private string convertGridToString()
    {
        string[] allLines = new string[maxRows + 2];
        
        for (int j = -1; j <= maxRows; j++)
        {
            string currentLine = "";
            for(int i = -1; i <= columns; i++)
            {
                if(i == -1 || j == -1 || i == columns || j == maxRows)
                {
                    currentLine += '*';
                } else
                {
                    currentLine += tiles[i, j] == null ? " " : "#";
                }
                
            }
            allLines[maxRows - j ] = currentLine;
        }
        return String.Join(Environment.NewLine, allLines);
    }

    private DirtTile[,] createGridFromString(string gridString)
    {
        if (gridString != null && gridString.Length > 0) {
            string[] lines = gridString.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            int stringRows = lines.Length - 2;
            int stringColumns = lines[0].Length - 2;
            columns = stringColumns;
            maxRows = stringRows;
            DirtTile[,] tilesGrid = new DirtTile[stringColumns, stringRows];

            for (int j = stringRows; j > 0; j--)
            {
                for (int i = 1; i <= stringColumns; i++)
                {
                    int gridRowIndex = (stringRows - j);
                    char currentChar = lines[j][i];
                    if (currentChar == '#')
                    {
                        createAndAttachTile(i - 1, gridRowIndex, tilesGrid);
                    }
                }
            }
            return tilesGrid;
        }
        return null;
    }
}
