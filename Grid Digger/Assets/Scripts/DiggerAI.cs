﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Graphing;

public class DiggerAI : MonoBehaviour {

    float distToGround;
    private bool followingPath = false;
    public TileGrid tileGrid;
    public float tilesMovedPerSecond = 2;
    public float jumpHeightMultiplier = 6.7f;
    private bool jumping = false;
    public float timeToWaitBetweenJumps = 0.1f;
    public float upwardsVelocityBeforeMoving = 8f;
    private float unitsPerSecond;
    private float yPositionWhenJumping;
    private float jumpTileWidthAboveTileBeforeMoving;
    public float jumpAmountBeforeMovingHorizontally = 0;
    private bool newPath = false;
    private bool broken = false;
    private Vector3? currentTargetLocation;
    private AStarNode currentPath;
    private float lastMoveTime;
    public float secondsBetweenMovements = 3;
    private Vector2Int? randomisedTile;
    System.Random random = new System.Random();


    // Use this for initialization
    void Start () {
        distToGround = GetComponent<Collider2D>().bounds.extents.y;
        float diggerHeight = GetComponent<SpriteRenderer>().sprite.bounds.size.y * transform.localScale.y;
        jumpTileWidthAboveTileBeforeMoving = diggerHeight + (diggerHeight * jumpAmountBeforeMovingHorizontally);
        jumpTileWidthAboveTileBeforeMoving = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (!followingPath)
        {
            if(Time.time - lastMoveTime > secondsBetweenMovements)
            {
                List<Vector2Int> walkableTiles = tileGrid.getAllWalkableTilesFromPosition(transform.position.x, transform.position.y);
                if(walkableTiles.Count > 0)
                {
                    Vector2Int randomWalkableTile = walkableTiles[random.Next(walkableTiles.Count)];
                    Debug.Log("Random tile found: " + randomWalkableTile);
                    Vector3 randomTileWorldPosition = tileGrid.getWorldLocation(randomWalkableTile.x, randomWalkableTile.y);
                    randomisedTile = randomWalkableTile;
                    tileGrid.highlightTile(new Vector2Int(randomWalkableTile.x, randomWalkableTile.y - 1));
                    StartCoroutine(followPath(randomTileWorldPosition, true));
                }
            }
        }
	}

    public bool isGrounded()
    {
        LayerMask layerMask = LayerMask.GetMask("Dirt");
        return Physics2D.Raycast(transform.position, -Vector3.up, distToGround + 0.1f, layerMask);
    }

    public void gridUpdated(Vector2Int tileUpdated, bool tileAdded)
    {
        if (followingPath)
        {
            if (doesPathContainCoordinate(tileUpdated, tileAdded))
            {
                StartCoroutine(followPath(currentTargetLocation.Value, randomisedTile.HasValue));
            }
        }
    }

    private bool doesPathContainCoordinate(Vector2Int coordinate, bool tileAdded)
    {
        AStarNode path = currentPath;
        if (!tileAdded)
        {
            while (path != null)
            {
                if (path.data.x == coordinate.x && path.data.y == coordinate.y + 1)
                {
                    return true;
                }
                path = path.child;
            }
            return false;
        } else
        {
            return true; //if a tile has been added, we need to recalculate the path to check if jumps etc have been blocked
        }
    }

    public IEnumerator followPath(Vector3 targetLoc, bool wasRandomised)
    {
        if (!wasRandomised && randomisedTile.HasValue)
        {
            tileGrid.disableHighlightTile(new Vector2Int(randomisedTile.Value.x, randomisedTile.Value.y - 1));
            randomisedTile = null;
        }
        Debug.Log("Coroutine called");
        if (!followingPath)
        {
            followingPath = true;
            currentTargetLocation = targetLoc;
            yield return StartCoroutine(moveToLocation(targetLoc));
        } else
        {
            newPath = true;
            broken = false;
            while (!broken) //wait for running coroutine to finish and stop pathfinding.
            {
                yield return null;
            }
            newPath = false;
            followingPath = true;
            broken = false;
            currentTargetLocation = targetLoc;
            yield return StartCoroutine(moveToLocation(targetLoc));
        }
    }

    IEnumerator waitForNextFrame()
    {
        yield return null;
    }

    IEnumerator moveToLocation(Vector3 targetLoc)
    {
        while (!isGrounded())
        {
            yield return null;
        }
        if (newPath)
        {
            broken = true;
            yield break;
        }
        Debug.Log("Beginning movement now");
        AStarNode path = tileGrid.getPathToTile(transform.position.x, transform.position.y, targetLoc.x, targetLoc.y);
        currentPath = path;
        while(path != null)
        {
            //Debug.Log("Heading to tile: " + path.data.x + ", " + path.data.y + " at world position: " + path.data.worldX + ", " + path.data.worldY);
            if (atCorrectTilePosition(path) && isGrounded())
            {
                if (newPath)
                {
                    break;
                }
                else
                {
                    path = path.child;
                    currentPath = path;
                    continue;
                }
            }

            //if the next tile is absolutely to the left or right, and we're not falling, just move closer towards it
            if (atCorrectHeight(path) && isOneTileAwayHorizontally(path) && isGrounded()) 
            {
                moveLeftOrRight(path);
                yield return null;
                continue;
            }


            //if the next tile is to the left or right, but below, just move to it
            if (fallToTile(path) && isGrounded())
            {
                moveLeftOrRight(path);
                yield return null;
                continue;
            }


            //if just falling, continue moving left/right to get to the right place
            if (!isGrounded() && !isHeadingUpwards())
            {
                moveLeftOrRight(path);
                yield return null;
                continue;
            }

            //if the tile is to the right/left but above, do a jump
            if (isOneTileBelowTarget(path) && isGrounded() && !isHeadingUpwards())
            {
                yield return StartCoroutine(jump());
                yield return null;
                continue;
            }

            if (jumping)
            {
                yield return null;
                continue;
            }

            //if the tile is 2 or more tiles to the right or the left, just do a jump and move right/left
            if (isTwoOrMoreTilesAwayHorizontally(path) && isGrounded() &&!jumping)
            {
                yield return StartCoroutine(jump());
                yield return null;
                continue;
            }


            break;

        }
        if (newPath)
        {
            Debug.Log("COROUTINE ENDED BECAUSE NEW PATH");
        }
        else
        {
            Debug.Log("At destination");
            if (randomisedTile != null)
            {
                tileGrid.disableHighlightTile(new Vector2Int(randomisedTile.Value.x, randomisedTile.Value.y - 1));
                randomisedTile = null;
            }
        }
        followingPath = false;
        newPath = false;
        broken = true;
        currentTargetLocation = null;
        currentPath = null;
        lastMoveTime = Time.time;
        yield break;
    }

    private bool isTwoOrMoreTilesAwayHorizontally(AStarNode target)
    {
        return (Mathf.Abs(transform.position.x - target.data.worldX) > (tileGrid.getTileWidth() * 0.5));
    }

    private bool isHeadingUpwards()
    {
        float heightToReach = (yPositionWhenJumping + tileGrid.getTileWidth() + jumpTileWidthAboveTileBeforeMoving);
        return transform.position.y < heightToReach && GetComponent<Rigidbody2D>().velocity.y > 0;
    }

    private void moveLeftOrRight(AStarNode target)
    {
        if(transform.position.x > target.data.worldX)
        {
            moveLeft(target);
        }
        else
        {
            moveRight(target);
        }
    }

    private void recalculateSpeed()
    {
        unitsPerSecond = tilesMovedPerSecond * tileGrid.getTileWidth();
    }

    private void moveLeft(AStarNode target)
    {
        recalculateSpeed();
        if ((transform.position.x - target.data.worldX) < (unitsPerSecond * Time.deltaTime))
        {
            transform.Translate(new Vector3(-(transform.position.x - target.data.worldX), 0, 0));
        }
        else
        {
            transform.Translate(new Vector3(-unitsPerSecond * Time.deltaTime, 0, 0));
        }
    }

    private void moveRight(AStarNode target)
    {
        recalculateSpeed();
        //Debug.Log("Distance from tile = " + (target.data.worldX - transform.position.x));
        if ((target.data.worldX - transform.position.x) < (unitsPerSecond * Time.deltaTime))
        {
            transform.Translate(new Vector3((target.data.worldX - transform.position.x), 0, 0));
        }
        else
        {
            transform.Translate(new Vector3(unitsPerSecond * Time.deltaTime, 0, 0));
        }
    }

    public IEnumerator jump()
    {
        if (!jumping)
        {
            //calculate the force needed to get to 1.5 times the tile height
            float g = GetComponent<Rigidbody2D>().gravityScale * Physics2D.gravity.magnitude;
            float maxJump = tileGrid.getTileWidth() + (tileGrid.getTileWidth() / 2);
            float v2 = maxJump * 2 * g;
            float force = Mathf.Sqrt(v2) * GetComponent<Rigidbody2D>().mass;

            yield return new WaitForSeconds(timeToWaitBetweenJumps);
            yPositionWhenJumping = transform.position.y;
            GetComponent<Rigidbody2D>().AddForce(new Vector3(0, force, 0), ForceMode2D.Impulse);
            jumping = true;
        }
    }

    private bool isOneTileBelowTarget(AStarNode tile)
    {
        return (tile.data.worldY - transform.position.y - (tile.data.worldY * 0.2) > 0);
    }

    private bool isOneTileAwayHorizontally(AStarNode tile)
    {
        return (Mathf.Abs(transform.position.x - tile.data.worldX) <= (tileGrid.getTileWidth() + (tileGrid.getTileWidth() * 0.2)));
    }

    private bool fallToTile(AStarNode tile)
    {
        return isOneTileAwayHorizontally(tile) && isOneOrMoreTileAboveTarget(tile);
    }

    private bool isOneOrMoreTileAboveTarget(AStarNode tile)
    {
        return (transform.position.y - tile.data.worldY - (tileGrid.getTileWidth() * 0.2) > 0);
    }

    private bool atCorrectHeight(AStarNode tile)
    {
        return (Mathf.Abs(transform.position.y - tile.data.worldY) < tileGrid.getTileWidth());
    }

    private bool atCorrectTilePosition(AStarNode tile)
    {
        if(atCorrectHeight(tile))
        {
            return transform.position.x == tile.data.worldX;
        }
        return false;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Dirt")
        {
            jumping = false;
        }
    }
}
