﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DirtTile : MonoBehaviour {

    public float maxHealth = 100;
    public float currentHealth;
    public int x;
    public int y;
    public Image healthBar;
    public Canvas healthBarContainer;

	// Use this for initialization
	void Start () {
        this.currentHealth = maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public float damage(float damageAmount)
    {
        currentHealth -= damageAmount;
        if(currentHealth < maxHealth)
        {
            healthBarContainer.enabled = true;
            healthBar.fillAmount = currentHealth / maxHealth;
        }
        
        
        return currentHealth;
    }
}
